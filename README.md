# Java Coding Assignment
### Note: Need to add jwt authentication
### Prerequisite
1. Docker/Docker-composer
2. Java (v8.x or above)
3. Gradle



### Step build app based on spring boot framework
- Define all dependencies in build.gradle
- Create a Spring Boot Main @SpringBootApplication
- Create components @Entity / @RestController / @Repository
- Create application.properties
- Build 
- Check app using curls 

### BUILD the application 
./gradlew build   

### BUILD AND UP Docker Compose 
```docker-compose up ```   
```docker-compose down <- down docker composec```

### Supported commands     
1. Start docker composer: ``` docker-composer start ```
2. Run migration and seed: ``` gradle flywayMigrate -i ```
3. Run build: ```./gradlew build ```
4. Run application: ``` java -jar ./build/libs/shopping-0.0.1-SNAPSHOT.jar```
3. Run test: ``` gradle test -i ```

### Folder Structure
#### ./src/main/resources/application.properties 
> Define configuration for project
#### ./src/main/resources/db/migration
> Migration folder
#### ./src/main/java/com/nab/shopping/configuration
> Define configuration application such as redis and rabbitMq
#### ./src/main/java/com/nab/shopping/controller
> Restful Api Controller
#### ./src/main/java/com/nab/shopping/exception
> Extension exception class
#### ./src/main/java/com/nab/shopping/model
> Entity and model for Restful API
#### ./src/main/java/com/nab/shopping/repository
> Data access function
#### ./src/main/java/com/nab/shopping/service
> Service class for project
### CURLS 

#### POST /authenticate
```$
curl -X POST \
  http://localhost:8080/authenticate \
  -H 'Host: localhost:8080' \
  -H 'Content-Type: application/json' \
  -d '{
	"username":"user",
	"password": "password"
}'
```
#### POST /user/save 
```
curl -s -X POST \
  http://localhost:8080/user/save \
  -H 'Content-Type: application/json' \
  -d '{"username":"Your Name",
        "password":"password"
  }'
```

#### GET /user/{id}
```
curl -s -X GET \
  http://localhost:8080/user/1 
```
#### POST /products
```
curl -X POST \
  http://localhost:8080/products \
  -H 'Content-Type: application/json' \
  -H 'Host: localhost:8080' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyIiwiZXhwIjoxNTkwNzUzODA1LCJpYXQiOjE1OTA3MzU4MDV9.ivztWH_9_hPENfyJ7pAEIJMXbvkHxAvDFP7njsAZbFoBkncQ9sb-htGRGLvXnmqRoJv3UfPuMxHow8uoN06GEQ' \
  -d '{
	"name":"product1",
	"description": "The main product 1 is very good",
	"brand":"ZARA",
	"color":"Red"
}'
```
#### PUT /products/{product_id}
```
curl -X PUT \
  http://localhost:8080/products \
  -H 'Content-Type: application/json' \
  -H 'Host: localhost:8080' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyIiwiZXhwIjoxNTkwNzUzODA1LCJpYXQiOjE1OTA3MzU4MDV9.ivztWH_9_hPENfyJ7pAEIJMXbvkHxAvDFP7njsAZbFoBkncQ9sb-htGRGLvXnmqRoJv3UfPuMxHow8uoN06GEQ' \
  -d '{
	"name":"product1",
	"description": "The main product 1 is very good",
	"brand":"ZARA",
	"color":"Red"
}'
```
#### GET /products
```
curl -X GET \
  http://localhost:8080/products \
  -H 'Content-Type: application/json' \
  -H 'Host: localhost:8080' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyIiwiZXhwIjoxNTkwNzUzODA1LCJpYXQiOjE1OTA3MzU4MDV9.ivztWH_9_hPENfyJ7pAEIJMXbvkHxAvDFP7njsAZbFoBkncQ9sb-htGRGLvXnmqRoJv3UfPuMxHow8uoN06GEQ' \
```
#### POST /products/{product_id}/prices
```
curl -X POST \
  http://localhost:8080/products/1/prices \
  -H 'Content-Type: application/json' \
  -H 'Host: localhost:8080' \
  H 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyIiwiZXhwIjoxNTkwNzUzODA1LCJpYXQiOjE1OTA3MzU4MDV9.ivztWH_9_hPENfyJ7pAEIJMXbvkHxAvDFP7njsAZbFoBkncQ9sb-htGRGLvXnmqRoJv3UfPuMxHow8uoN06GEQ' \
  -d '{
	"price":1.4
}'
```
#### PUT /products/{product_id}/prices/{price_id}
```
curl -X PUT \
  http://localhost:8080/products/1/prices/2 \
  -H 'Content-Type: application/json' \
  -H 'Host: localhost:8080' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyIiwiZXhwIjoxNTkwNzUzODA1LCJpYXQiOjE1OTA3MzU4MDV9.ivztWH_9_hPENfyJ7pAEIJMXbvkHxAvDFP7njsAZbFoBkncQ9sb-htGRGLvXnmqRoJv3UfPuMxHow8uoN06GEQ' \
  -d '{
	"price":1.4
}'
```
#### GET /products/{filer_name}/filter_name
```
curl -X GET \
  http://localhost:8080/products/oduc/filter_name \
  -H 'Host: localhost:8080' \
```
### API flow diagram
#### User:
![Diagram](public/sequence_user.png)

#### Admin:
![Diagram](public/sequence_admin.png)
### Database diagram
![Diagram](public/DatabaseDiagram.png)
### Reference Documentation
For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.3.0.RELEASE/gradle-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.3.0.RELEASE/gradle-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.3.0.RELEASE/reference/htmlsingle/#boot-features-developing-web-applications)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)

### Additional Links
These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)

