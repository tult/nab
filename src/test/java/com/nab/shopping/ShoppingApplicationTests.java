package com.nab.shopping;

import org.junit.jupiter.api.Test;

import org.junit.runner.RunWith;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * The type Shopping application tests.
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = {
        "spring.jpa.hibernate.ddl-auto=create-drop",
        "spring.liquibase.enabled=false",
        "spring.flyway.enabled=false"
})
@RunWith(SpringRunner.class)
class ShoppingApplicationTests {

    /**
     * Context loads.
     */
    @Test
    void contextLoads() {
    }

}
