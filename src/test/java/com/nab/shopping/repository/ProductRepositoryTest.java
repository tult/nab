package com.nab.shopping.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit4.SpringRunner;

import com.nab.shopping.model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


/**
 * The type Product repository test.
 */
@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
public class ProductRepositoryTest {
    /**
     * The Product repository.
     */
    @Mock
    ProductRepository productRepository;

    /**
     * Sets up.
     */
    @Before
    public void setUp() {
        // given
        Product product = new Product();
        product.setId(1L);
        product.setName("HM");
        product.setDescription("HM desc");
        List<Product> list = new ArrayList<Product>();
        list.add(product);
        when(productRepository.findAll()).thenReturn(list);
        when(productRepository.save(any(Product.class))).thenReturn(product);
        when(productRepository.findById(1L)).thenReturn(Optional.of(list.get(0)));

    }

    /**
     * When find by id then return product.
     */
    @Test
    public void whenFindById_thenReturnProduct() {
        // when
        Product product = productRepository.findById(1L).get();
        // then
        assertThat(product.getDescription()).isEqualTo("HM desc");
    }

    /**
     * When find all then return product.
     */
    @Test
    public void whenFindAll_thenReturnProduct() {
        // when
        List<Product> products = productRepository.findAll();
        // then
        assertThat(products.size()).isEqualTo(1);
    }
}
