package com.nab.shopping.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.nab.shopping.model.Product;
import com.nab.shopping.model.Price;

import com.nab.shopping.repository.ProductRepository;
import com.nab.shopping.repository.PriceRepository;
import com.nab.shopping.repository.RedisRepository;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * The type Price controller unit tests.
 */
@ExtendWith(MockitoExtension.class)
public class PriceControllerUnitTests {
    /**
     * The Price controller.
     */
    @InjectMocks
    PriceController priceController;

    /**
     * The Product repository.
     */
    @Mock
    ProductRepository productRepository;

    /**
     * The Price repository.
     */
    @Mock
    PriceRepository priceRepository;

    /**
     * The Redis repository.
     */
    @Mock
    RedisRepository redisRepository;

    /**
     * Create price.
     */
    @Test
    public void createPrice() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        Product product = new Product();
        product.setId(1L);
        product.setName("HM");
        product.setDescription("HM desc");
        List<Product> list = new ArrayList<Product>();
        list.add(product);
        Price price = new Price();
        price.setId(1L);
        price.setProduct(product);
        when(productRepository.findById(1L)).thenReturn(Optional.of(list.get(0)));
        when(priceRepository.save(any(Price.class))).thenReturn(price);
        Price priceEntity = priceController.createPrice(1L, price);
        assertThat(priceEntity.getId()).isEqualTo(1);
        assertThat(priceEntity.getProduct().getName()).isEqualTo("HM");

    }
}
