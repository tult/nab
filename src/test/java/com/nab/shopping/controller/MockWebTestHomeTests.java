package com.nab.shopping.controller;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * The type Mock web test home tests.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MockWebTestHomeTests {
    @Autowired
    private MockMvc mvc;

    /**
     * Example test.
     *
     * @throws Exception the exception
     */
    @Test
    public void exampleTest() throws Exception {
        this.mvc.perform(get("/home")).andExpect(status().isOk())
                .andExpect(content().string("Hello Spring Boot with Docker"));
    }
}
