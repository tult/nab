package com.nab.shopping.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.nab.shopping.model.Product;
import com.nab.shopping.repository.ProductRepository;
import com.nab.shopping.repository.RedisRepository;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * The type Product controller unit tests.
 */
@ExtendWith(MockitoExtension.class)
public class ProductControllerUnitTests {
    /**
     * The Product controller.
     */
    @InjectMocks
    ProductController productController;

    /**
     * The Product repository.
     */
    @Mock
    ProductRepository productRepository;

    /**
     * The Redis repository.
     */
    @Mock
    RedisRepository redisRepository;

    /**
     * Test add product.
     */
    @Test
    public void testAddProduct() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        Product product = new Product();
        product.setId(1L);
        when(productRepository.save(any(Product.class))).thenReturn(product);

        Product productToAdd = new Product();
        productToAdd.setName("HM");
        productToAdd.setDescription("HM Description");
        productToAdd.setBrand("hm");
        productToAdd.setColor("Red");
        Product responseEntity = productController.createPost(productToAdd);
        assertThat(responseEntity.getId()).isEqualTo(1);
    }

    /**
     * Test find all.
     */
    @Test
    public void testFindAll() {
        Product productToAdd = new Product();
        productToAdd.setName("HM");
        List<Product> list = new ArrayList<Product>();
        list.add(productToAdd);
        when(productRepository.findAll()).thenReturn(list);

        // when
        List<Product> results = productController.getAllPosts();
        assertThat(results.size()).isEqualTo(1);

    }
}
