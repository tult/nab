create table users
(
  id          bigserial not null
    constraint users_pkey
      primary key,
  created_at  timestamp not null,
  updated_at  timestamp not null,
  email       varchar(255),
  facebook_id varchar(255),
  password    varchar(255),
  role        integer   not null,
  username    varchar(255)
);

alter table users
  owner to postgres;

create table product
(
  id          bigserial    not null
    constraint product_pkey
      primary key,
  created_at  timestamp    not null,
  updated_at  timestamp    not null,
  brand       varchar(100) not null,
  color       varchar(100) not null,
  description varchar(250) not null,
  name        varchar(100) not null
    constraint uk_jmivyxk9rmgysrmsqw15lqr5b
      unique
);

alter table product
  owner to postgres;

create table price
(
  id         bigserial        not null
    constraint price_pkey
      primary key,
  created_at timestamp        not null,
  updated_at timestamp        not null,
  price      double precision not null,
  product_id bigint
    constraint fkk4mbgqf5yru5ib5b6w5l6ukkj
      references product
);

alter table price
  owner to postgres;
