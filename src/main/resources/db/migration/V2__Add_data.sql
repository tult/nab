insert into users(id,username,facebook_id,password,role, created_at, updated_at) values (1,'admin','fb_1234','$2a$10$VyLRFL1zcZsqwXC8vEpxf.j1dRo9idr7kWYwqFxRRi38.N03K41be',1,current_timestamp,current_timestamp);
insert into users(id,username,facebook_id,password,role, created_at, updated_at) values (2,'user','fb_1234','$2a$10$VyLRFL1zcZsqwXC8vEpxf.j1dRo9idr7kWYwqFxRRi38.N03K41be',2,current_timestamp,current_timestamp);

insert into product(id,brand,created_at,updated_at,color,description,name) values (1,'BR1',current_timestamp,current_timestamp,'COLOR1','DES 1','NAME 1');
insert into price(price,product_id,created_at,updated_at) values (2.0,1,current_timestamp,current_timestamp);

insert into product(id,brand,created_at,updated_at,color,description,name) values (2,'BR2',current_timestamp,current_timestamp,'COLOR2','DES 2','NAME 2');
insert into price(price,product_id,created_at,updated_at) values (2.2,2,current_timestamp,current_timestamp);

insert into product(id,brand,created_at,updated_at,color,description,name) values (3,'BR3',current_timestamp,current_timestamp,'COLOR3','DES 3','NAME 3');
insert into price(price,product_id,created_at,updated_at) values (2.3,3,current_timestamp,current_timestamp);


