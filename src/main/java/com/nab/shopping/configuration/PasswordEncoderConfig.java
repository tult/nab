package com.nab.shopping.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * The type Password encoder config.
 */
@Configuration
public class PasswordEncoderConfig {
    /**
     * Gets encoder.
     *
     * @return the encoder
     */
    @Bean
    PasswordEncoder getEncoder() {
        return new BCryptPasswordEncoder();
    }
}
