package com.nab.shopping.configuration;

public class Util {
    public static final String ACTIVITY_POST_PRODUCT = "POST-PRODUCT";
    public static final String ACTIVITY_POST_PRODUCT_MESSGE = "Start create product from user";
    public static final String ACTIVITY_GET_PRODUCT = "GET-PRODUCT";
    public static final String ACTIVITY_GET_PRODUCT_MESSGE = "Start get product from user";
    public static final String ACTIVITY_PUT_PRODUCT = "PUT-PRODUCT";
    public static final String ACTIVITY_PUT_PRODUCT_MESSGE = "Start edit product from user";
    public static final String ACTIVITY_POST_PRICE = "POST-PRICE";
    public static final String ACTIVITY_POST_PRICE_MESSAGE = "Start create new price for product from user";
    public static final String ACTIVITY_PUT_PRICE = "PUT-PRICE";
    public static final String ACTIVITY_PUT_PRICE_MESSAGE = "Start edit the price for product from user";

    public static final String ERROR_PUT_PRODUCT = "ERROR-PUT-PRODUCT";
    public static final String ERROR_PUT_PRODUCT_MESSAGE = "Error put product";
    public static final String ERROR_LOG_METHOD_MESSAGE = "Error in method %s";
    public static final String ERROR_RESOURCE_PRODUCT_NOT_FOUND = "Error resource product %d not found";
}
