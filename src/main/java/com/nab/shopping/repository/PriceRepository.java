package com.nab.shopping.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.nab.shopping.model.Price;

import java.util.*;

/**
 * The interface Price repository.
 */
@Repository
public interface PriceRepository extends JpaRepository<Price, Long> {
    /**
     * Find by product id list.
     *
     * @param productId the product id
     * @return the list
     */
    List<Price> findByProductId(Long productId);

    /**
     * Find by id and product id optional.
     *
     * @param id        the id
     * @param productId the product id
     * @return the optional
     */
    Optional<Price> findByIdAndProductId(Long id, Long productId);
}
