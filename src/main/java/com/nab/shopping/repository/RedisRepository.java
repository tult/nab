package com.nab.shopping.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import java.util.Iterator;

/**
 * The type Redis repository.
 */
@Repository
public class RedisRepository {
    private RedisTemplate<String, String> redisTemplate;

    /**
     * Instantiates a new Redis repository.
     *
     * @param redisTemplate the redis template
     */
    public RedisRepository(RedisTemplate<String, String> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    /**
     * Gets value.
     *
     * @param key the key
     * @return the value
     */
    public Object getValue(final String key) {
        return redisTemplate.opsForValue().get(key);
    }

    /**
     * Sets value.
     *
     * @param key   the key
     * @param value the value
     */
    public void setValue(final String key, final String value) {
        Date date = new Date();
        redisTemplate.opsForValue().set(key + "-" + date.getTime(), value);
    }

    /**
     * Gets all values.
     *
     * @param key the key
     * @return the all values
     */
    public HashMap<String, String> getAllValues(String key) {
        HashMap<String, String> ret = new HashMap<String, String>();
        Set<String> redisKeys = redisTemplate.keys(key + "*");
        Iterator<String> it = redisKeys.iterator();
        while (it.hasNext()) {
            String iterValue = it.next();
            ret.put(iterValue, redisTemplate.opsForValue().get(iterValue));
        }
        return ret;
    }
}
