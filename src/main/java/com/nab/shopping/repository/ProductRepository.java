package com.nab.shopping.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.nab.shopping.model.Product;

import java.util.*;

/**
 * The interface Product repository.
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    /**
     * Find product by name is containing list.
     *
     * @param name the name
     * @return the list
     */
    List<Product> findProductByNameIsContaining(String name);
}
