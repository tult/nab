package com.nab.shopping.service;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.nab.shopping.model.Product;

/**
 * The type Rabbit mq sender.
 */
@Service
public class RabbitMQSender {
    @Autowired
    private AmqpTemplate rabbitTemplate;

    @Value("${shop.rabbitmq.exchange}")
    private String exchange;

    @Value("${shop.rabbitmq.routingkey}")
    private String routingkey;

    /**
     * Send.
     *
     * @param product the product
     */
    public void send(Product product) {
        rabbitTemplate.convertAndSend(exchange, routingkey, product);
    }

    /**
     * Send string.
     *
     * @param msg the msg
     */
    public void sendString(String msg) {
        rabbitTemplate.convertAndSend(exchange, routingkey, msg);
    }
}
