package com.nab.shopping.controller;

import com.nab.shopping.model.User;
import com.nab.shopping.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * The type User controller.
 */
@RestController
public class UserController {

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    /**
     * Instantiates a new User controller.
     *
     * @param userRepository  the user repository
     * @param passwordEncoder the password encoder
     */
    @Autowired
    public UserController(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }


    /**
     * All iterable.
     *
     * @return the iterable
     */
    @GetMapping("/users")
    Iterable<User> all() {
        return userRepository.findAll();
    }

    /**
     * User by id user.
     *
     * @param id the id
     * @return the user
     */
    @GetMapping("/users/{id}")
    User userById(@PathVariable Long id) {
        return userRepository.findById(id).orElseThrow(() -> new ResponseStatusException(
                HttpStatus.NOT_FOUND));
    }

    /**
     * Save user.
     *
     * @param user the user
     * @return the user
     */
    @PostMapping("/users")
    User save(@RequestBody User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

}
