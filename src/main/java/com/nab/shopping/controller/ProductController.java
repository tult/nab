package com.nab.shopping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.nab.shopping.repository.ProductRepository;
import com.nab.shopping.model.Product;
import com.nab.shopping.exception.ResourceNotFoundException;
import com.nab.shopping.repository.RedisRepository;
import com.nab.shopping.configuration.Util;
import com.nab.shopping.service.RabbitMQSender;

import javax.validation.Valid;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Product controller.
 */
@RestController
public class ProductController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private ProductRepository productRepository;
    private RedisRepository redisRepository;
    private RabbitMQSender rabbitMQSender;

    /**
     * Instantiates a new Product controller.
     *
     * @param productRepository the product repository
     * @param redisRepository   the redis repository
     * @param rabbitMQSender    the rabbit mq sender
     */
    @Autowired
    public ProductController(ProductRepository productRepository, RedisRepository redisRepository, RabbitMQSender rabbitMQSender) {
        this.productRepository = productRepository;
        this.redisRepository = redisRepository;
        this.rabbitMQSender = rabbitMQSender;
    }

    /**
     * Gets all posts.
     *
     * @return the all posts
     */
    @GetMapping("/products")
    public List<Product> getAllPosts() {
        redisRepository.setValue(Util.ACTIVITY_GET_PRODUCT, Util.ACTIVITY_GET_PRODUCT_MESSGE);

        return productRepository.findAll();
    }

    /**
     * Gets all product filter name.
     *
     * @param filter_name the filter name
     * @return the all product filter name
     */
    @GetMapping("/products/{filter_name}/filter_name")
    public List<Product> getAllProductFilterName(@PathVariable String filter_name) {
        redisRepository.setValue(Util.ACTIVITY_GET_PRODUCT, Util.ACTIVITY_GET_PRODUCT_MESSGE);

        return productRepository.findProductByNameIsContaining(filter_name);
    }

    /**
     * Create post product.
     *
     * @param post the post
     * @return the product
     */
    @PostMapping("/products")
    public Product createPost(@Valid @RequestBody Product post) {
        redisRepository.setValue(Util.ACTIVITY_POST_PRODUCT, Util.ACTIVITY_POST_PRODUCT_MESSGE);

        return productRepository.save(post);
    }

    /**
     * Update post product.
     *
     * @param productId   the product id
     * @param postRequest the post request
     * @return the product
     */
    @PutMapping("/products/{productId}")
    public Product updatePost(@PathVariable Long productId, @Valid @RequestBody Product postRequest) {
        redisRepository.setValue(Util.ACTIVITY_PUT_PRODUCT, Util.ACTIVITY_PUT_PRODUCT_MESSGE);

        return productRepository.findById(productId).map(post -> {
            post.setName(postRequest.getName());
            post.setDescription(postRequest.getDescription());
            post.setBrand(postRequest.getBrand());
            return productRepository.save(post);
        }).orElseThrow(() ->
        {
            redisRepository.setValue(Util.ERROR_PUT_PRODUCT, Util.ERROR_PUT_PRODUCT_MESSAGE);
            rabbitMQSender.send(postRequest);
            logger.error(String.format(Util.ERROR_LOG_METHOD_MESSAGE, "updatePost"));
            return new ResourceNotFoundException(String.format(Util.ERROR_RESOURCE_PRODUCT_NOT_FOUND, productId));
        });
    }


    /**
     * Delete post response entity.
     *
     * @param productId the product id
     * @return the response entity
     */
    @DeleteMapping("/products/{productId}")
    public ResponseEntity<?> deletePost(@PathVariable Long productId) {
        return productRepository.findById(productId).map(post -> {
            productRepository.delete(post);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException(String.format(Util.ERROR_RESOURCE_PRODUCT_NOT_FOUND, productId)));
    }
}
