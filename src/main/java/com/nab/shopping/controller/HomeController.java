package com.nab.shopping.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The type Home controller.
 */
@RestController
public class HomeController {

    /**
     * Home string.
     *
     * @return the string
     */
    @RequestMapping("/home")
    public String home() {
        return "Hello Spring Boot with Docker";
    }

}
