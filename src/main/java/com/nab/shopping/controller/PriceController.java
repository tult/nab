package com.nab.shopping.controller;

import com.nab.shopping.configuration.Util;
import com.nab.shopping.repository.RedisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.nab.shopping.repository.PriceRepository;
import com.nab.shopping.repository.ProductRepository;
import com.nab.shopping.model.Price;
import com.nab.shopping.exception.ResourceNotFoundException;

import javax.validation.Valid;
import java.util.List;

/**
 * The type Price controller.
 */
@RestController
public class PriceController {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private PriceRepository priceRepository;

    @Autowired
    private RedisRepository redisRepository;

    /**
     * Gets courses by instructor.
     *
     * @param productId the product id
     * @return the courses by instructor
     */
    @GetMapping("/products/{productId}/prices")
    public List<Price> getCoursesByInstructor(@PathVariable(value = "productId") Long productId) {
        return priceRepository.findByProductId(productId);
    }

    /**
     * Create price price.
     *
     * @param productId the product id
     * @param price     the price
     * @return the price
     * @throws ResourceNotFoundException the resource not found exception
     */
    @PostMapping("/products/{productId}/prices")
    public Price createPrice(@PathVariable(value = "productId") Long productId,
                             @Valid @RequestBody Price price) throws ResourceNotFoundException {
        redisRepository.setValue(Util.ACTIVITY_POST_PRICE, Util.ACTIVITY_POST_PRICE_MESSAGE);

        return productRepository.findById(productId).map(product -> {
            price.setProduct(product);
            return priceRepository.save(price);
        }).orElseThrow(() -> new ResourceNotFoundException("instructor not found"));
    }

    /**
     * Update price price.
     *
     * @param productId    the product id
     * @param priceId      the price id
     * @param priceRequest the price request
     * @return the price
     * @throws ResourceNotFoundException the resource not found exception
     */
    @PutMapping("/products/{productId}/prices/{priceId}")
    public Price updatePrice(@PathVariable(value = "productId") Long productId,
                             @PathVariable(value = "priceId") Long priceId, @Valid @RequestBody Price priceRequest)
            throws ResourceNotFoundException {
        redisRepository.setValue(Util.ACTIVITY_PUT_PRICE, Util.ACTIVITY_PUT_PRICE_MESSAGE);

        if (!productRepository.existsById(productId)) {
            throw new ResourceNotFoundException("instructorId not found");
        }

        return priceRepository.findById(priceId).map(price -> {
            price.setPrice(priceRequest.getPrice());
            return priceRepository.save(price);
        }).orElseThrow(() -> new ResourceNotFoundException("course id not found"));
    }

    /**
     * Delete course response entity.
     *
     * @param productId the product id
     * @param priceId   the price id
     * @return the response entity
     * @throws ResourceNotFoundException the resource not found exception
     */
    @DeleteMapping("/products/{productId}/prices/{priceId}")
    public ResponseEntity<?> deleteCourse(@PathVariable(value = "productId") Long productId,
                                          @PathVariable(value = "priceId") Long priceId) throws ResourceNotFoundException {
        return priceRepository.findByIdAndProductId(priceId, productId).map(course -> {
            priceRepository.delete(course);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException(
                "Course not found with id " + priceId + " and instructorId " + productId));
    }
}
