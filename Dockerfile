FROM postgres:9.6
COPY pg-init-scripts/create-multiple-postgresql-databases.sh /docker-entrypoint-initdb.d/